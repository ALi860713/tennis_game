﻿using System;
using System.Collections.Generic;

namespace Tennis_Game
{
    public class Tennis
    {
        private int _firstPlayerScore;
        private int _secondPlayerScore;
        private Dictionary<int, string> _scoreLookUp = new Dictionary<int, string>()
        {
            {0, "Love"},
            {1, "Fifteen"},
            {2, "Thirty"},
            {3, "Forty"},
        };
        public string GetPlayerScore()
        {
            if (isSameScore())
            {
                return _firstPlayerScore > 2 ? "Deuce" : _scoreLookUp[_firstPlayerScore] + "_All";
            }
            else if (isGamePoint())
            {
                return String.Concat(GetAdvPlayer(), isAdv() ? "_Adv" : "_Win");
            }
            return _scoreLookUp[_firstPlayerScore] + "_" + _scoreLookUp[_secondPlayerScore];
        }

        private bool isAdv()
        {
            return Math.Abs(_firstPlayerScore - _secondPlayerScore) == 1;
        }

        private string GetAdvPlayer()
        {
            return _firstPlayerScore > _secondPlayerScore ? "FirstPlayer" : "SecondPlayer";
        }

        private bool isGamePoint()
        {
            return _firstPlayerScore > 3 || _secondPlayerScore > 3;
        }

        private bool isSameScore()
        {
            return _firstPlayerScore == _secondPlayerScore;
        }

        public void FirstPlayerGetScore()
        {
            _firstPlayerScore++;
        }

        public void SecondPlayerGetScore()
        {
            _secondPlayerScore++;
        }
    }
}
