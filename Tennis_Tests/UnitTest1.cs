using Microsoft.VisualStudio.TestTools.UnitTesting;
using Tennis_Game;

namespace Tennis_Tests
{
    [TestClass]
    public class UnitTest1
    {
        private Tennis _tennis = new Tennis();
        [TestMethod]
        public void Love_All()
        {
            Assert.AreEqual("Love_All", _tennis.GetPlayerScore());
        }

        [TestMethod]
        public void Fifteen_Love()
        {
            GivenFirstPlayerScore(1);
            Assert.AreEqual("Fifteen_Love", _tennis.GetPlayerScore());
        }

        [TestMethod]
        public void Thirty_Love()
        {
            GivenFirstPlayerScore(2);
            Assert.AreEqual("Thirty_Love", _tennis.GetPlayerScore());
        }

        [TestMethod]
        public void Forty_Love()
        {
            GivenFirstPlayerScore(3);
            Assert.AreEqual("Forty_Love", _tennis.GetPlayerScore());
        }

        [TestMethod]
        public void Love_Fifteen()
        {
            GivenSecondPlayerScore(1);
            Assert.AreEqual("Love_Fifteen", _tennis.GetPlayerScore());
        }

        [TestMethod]
        public void Fifteen_All()
        {
            GivenFirstPlayerScore(1);
            GivenSecondPlayerScore(1);
            Assert.AreEqual("Fifteen_All", _tennis.GetPlayerScore());
        }

        [TestMethod]
        public void Deuce()
        {
            GivenFirstPlayerScore(3);
            GivenSecondPlayerScore(3);
            Assert.AreEqual("Deuce", _tennis.GetPlayerScore());
        }

        [TestMethod]
        public void FirstPlayer_Adv()
        {
            GivenFirstPlayerScore(4);
            GivenSecondPlayerScore(3);
            Assert.AreEqual("FirstPlayer_Adv", _tennis.GetPlayerScore());
        }

        [TestMethod]
        public void SecondPlayer_Adv()
        {
            GivenFirstPlayerScore(3);
            GivenSecondPlayerScore(4);
            Assert.AreEqual("SecondPlayer_Adv", _tennis.GetPlayerScore());
        }

        [TestMethod]
        public void FirstPlayer_Win()
        {
            GivenFirstPlayerScore(5);
            GivenSecondPlayerScore(3);
            Assert.AreEqual("FirstPlayer_Win", _tennis.GetPlayerScore());
        }

        [TestMethod]
        public void SecondPlayer_Win()
        {
            GivenFirstPlayerScore(3);
            GivenSecondPlayerScore(5);
            Assert.AreEqual("SecondPlayer_Win", _tennis.GetPlayerScore());
        }

        private void GivenFirstPlayerScore(int timer)
        {
            for (int i = 0; i < timer; i++)
            {
                _tennis.FirstPlayerGetScore();
            }
        }

        private void GivenSecondPlayerScore(int timer)
        {
            for (int i = 0; i < timer; i++)
            {
                _tennis.SecondPlayerGetScore();
            }
        }
    }
}
